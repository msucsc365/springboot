package edu.missouristate.controllers;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class HomeController {

	@ResponseBody
	@GetMapping("/string")
	public String getString() {
		//System.out.println("hi");
		return "<strong>Hello World!</strong>";
	}
	
	@GetMapping("/home")
	public String getHome() {
		//System.out.println("hi");
		return "home.jsp";
	}
	
	// Handles posts to the server (usually forms)
	// Could be AJAX calls as well
	@PostMapping("/home")
	public String postHome() {
		System.out.println("hi");
		return "home.jsp";
	}
	
//	// GET AND POST PROCESSING
//	// DON'T RECOMMEND
//	@RequestMapping("home")
//	public String getHome() {
//		System.out.println("hi");
//		return "home.jsp";
//	}
	
	
}
